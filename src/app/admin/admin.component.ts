import { Component, OnInit } from '@angular/core';
import { CourseServiceService } from '../course-service.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  managers = ['kiran', 'harish', 'ghk'];
  managerSelected:string;
  employees=[];
  filteredemployees=[];

  constructor(private service: CourseServiceService) {   
  }

  ngOnInit() {
  }
  getAllEmployeeData(selectedManager){
    this.filteredemployees=[];
    this.managerSelected=selectedManager;
    var url = 'http://localhost:5000/NodeCrupOP/api/getAllEmployeeData'
    this.service.getAllEmpoyeeData(url)
      .subscribe(
        (data:any) => {
          console.log(data);
        this.employees = data.data;
        for(let i=0; i < this.employees.length; i++){
if(this.employees[i].reportingManager==selectedManager){
  this.filteredemployees.push(this.employees[i]);
}
        }
      },err=>{
        console.log(err)
      })
  }

}
