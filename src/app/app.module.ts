import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { CoursesComponent } from './courses/courses.component';

import { NavbarComponent } from './navbar/navbar.component';
import { CourseServiceService } from './course-service.service';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MessageService } from '../message.service';
import { EmployeeComponent } from './employee/employee.component';
import { ProjectsComponent } from './projects/projects.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TablesComponent } from './tables/tables.component';
import { TopnavbarComponent } from './topnavbar/topnavbar.component';
import { TopnavComponent } from './topnav/topnav.component';
import { SidenavComponent } from './sidenav/sidenav.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    CoursesComponent, 
    NavbarComponent,
    HeaderComponent,
    FooterComponent,
    EmployeeComponent,
    ProjectsComponent,
    SidebarComponent,
    DashboardComponent,
    TablesComponent,
    TopnavbarComponent,
    TopnavComponent,
    SidenavComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,

    RouterModule.forRoot([
      { path: 'Admin', component: AdminComponent },      
      { path: 'Employee', component: EmployeeComponent },
      { path: 'Student/id', component: CoursesComponent },
    ])

  ],

  providers: [CourseServiceService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
