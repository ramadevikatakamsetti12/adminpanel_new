import { Component, OnInit } from '@angular/core';
import { CourseServiceService } from '../course-service.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {


  title = "Employee Information";
  employees=[];
  
  employee = {
    first_Name: '',
    last_Name: '',
    email: '',
    gender: '',
    Joining_Date: '',
    mobile: '',
    project: '',
    reportingManager:'',
  }
  managers=['Kiran','Harish'];

  Projects=['LMS','VegaWallet'];
  
  constructor(private service: CourseServiceService) { 

  }

  //Get all Project Data

  getAllProjects(){
    var url = 'http://localhost:5000/NodeCrupOP/api/getAllProjects'
    this.service.getAllProjects(url)
      .subscribe(
        (data:any) => {
          console.log(data);
        this.Projects= data.data;       
                  
      },err=>{
        console.log(err)
      })
  }

  //Get all Employees Data
  getAllEmployeeData(){
    var url = 'http://localhost:5000/NodeCrupOP/api/getAllEmployeeData'
    this.service.getAllStudentData(url)
      .subscribe(
        (data:any) => {
          console.log(data);
        this.employees = data.data;
        for(let i=0; i < this.employees.length; i++){
          if(this.employees[i].designation=='Reporting Manager'){
            this.managers.push(this.employees[i]);
          }
                  }
      },err=>{
        console.log(err)
      })
  }
  SaveEmployeeData() {
    var url='http://localhost:5000/NodeCrupOP/api/insertEmployee';
    console.log(this.employee);
    this.service.insertEmployee(url,this.employee)
      .subscribe(
        data => {
          console.log("Request is successful ", data);
          console.log(data);
         // this.getAllStudentData();
        },
        error => {
          console.log("Error", error);
        }
      );
  }
  ngOnInit() {
    this.getAllEmployeeData();

  }
  
}
