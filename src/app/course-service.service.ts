
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { observable } from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class CourseServiceService {

  
constructor (private http:HttpClient){

}
getAllStudentData(url){
  return this.http.get(url); 
}
 
getAllProjects(url){
return this.http.get(url);
}
 insertStudent(url,data){
  return this.http.post(url,data);

 }
 UpdateStudentData(url,data){
  return this.http.post(url,data);

 }

 //DeleteStudentById
 DeleteStudentById(url,data){
  return this.http.post(url,data);

 }
 //Insert employee data
 insertEmployee(url,data){
  return this.http.post(url,data);

 }

 getAllEmpoyeeData(url){
  return this.http.get(url); 
}
}
