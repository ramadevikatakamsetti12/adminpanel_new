import { Component, OnInit,OnDestroy } from '@angular/core';
import { CourseServiceService } from '../course-service.service';
import { Subscription } from 'rxjs';
import {MessageService} from '../../message.service';


@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnDestroy {
  message: any;
  subscription: Subscription;

  title="List of Courses";
  courses;

  constructor(service:CourseServiceService,service1:MessageService){
   // this.courses=service.getCourses();
    this.subscription=service1.getMessage().subscribe(message=>{this.message=message});
   // alert(this.message.text);
  }
  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
}

}
