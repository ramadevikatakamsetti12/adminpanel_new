import { Component, OnInit } from '@angular/core';
import { CourseServiceService } from '../course-service.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  display = 'none'; //default Variable
  title = "Students Information";
  students=[];
  isInline: boolean = false;
  showBtn=-2;
  student = {
    first_Name: '',
    last_Name: '',
    email: '',
    gender: '',
    Joining_Date: '',
    mobile: '',
    course: '',
  }
  
  
  constructor(private service: CourseServiceService) {   
  }
  getAllStudentData(){
    var url = 'http://localhost:5000/NodeCrupOP/api/getAllStudentData'
    this.service.getAllStudentData(url)
      .subscribe(
        (data:any) => {
          console.log(data);
        this.students = data.data;
      },err=>{
        console.log(err)
      })
  }
  saveStudent() {
    var url='http://localhost:5000/NodeCrupOP/api/saveData';
    console.log(this.student);
    this.service.insertStudent(url,this.student)
      .subscribe(
        data => {
          console.log("Request is successful ", data);
          console.log(data);
          this.getAllStudentData();
        },
        error => {
          console.log("Error", error);
        }
      );
  }
  
studentData={};

  EditData(studentdata,ind){
    this.showBtn = ind;
    this.studentData=studentdata;
  }
  updateStudent(student) {

    var url='http://localhost:5000/NodeCrupOP/api/UpdateStudentData';
    console.log(student);
    this.service.insertStudent(url,student)
      .subscribe(
        data => {
          console.log("Request is successful ", data);
          console.log(data);
          this.showBtn=-1;
          this.getAllStudentData();
        },
        error => {
          console.log("Error", error);
        }
      );
  }
  
  deleteStudent(student) {
    var url='http://localhost:5000/NodeCrupOP/api/DeleteStudentById';
    console.log(student);
    this.service.insertStudent(url,student)
      .subscribe(
        data => {
          console.log("Request is successful ", data);
          console.log(data);
          this.showBtn=-1;
          this.getAllStudentData();
        },
        error => {
          console.log("Error", error);
        }
      );
  }

  openModalDialog() {
    this.display = 'block'; //Set block css
  }

  closeModalDialog() {
    this.display = 'none'; //set none css after close dialog
  }

  ngOnInit() {
    this.getAllStudentData();
  }

}
